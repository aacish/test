package algorithm;

import java.util.List;

import entity.Order;

public class FindWaitingTime {
	public List<Order> populateWaitingTime(List<Order> orders){
		
		//calculate end time absolute time of a order ending
		for(int i = 0; i<orders.size();i++) {
			if(i==0) {
				Double ex = orders.get(i).getExecDuration();
				orders.get(i).setEndTime(ex.longValue());
			}
			else {
				Double ex = orders.get(i).getExecDuration();
				long end = orders.get(i-1).getEndTime()+ex.longValue();
				orders.get(i).setEndTime(end);
			}
		}
		
		//calculate waiting time
		for(int i=0;i<orders.size();i++) {
			if(i==0) {
				Double ex = orders.get(i).getExecDuration();
				orders.get(i).setWaitingTime(ex.longValue());
			}
			else {
				long defferd = orders.get(i).getEndTime()-orders.get(i).getArrTime();
				orders.get(i).setWaitingTime(defferd);
			}
			 
		}
		
		return orders;
	}
	
	
	
	public double getAverageWaitingTime(List<Order> orders) {
		long total = 0;
		if(orders!=null) {
			if(orders.size()>0) {
				for(Order order: orders) {
					total=total+order.getWaitingTime();
				}
			}
		}
		
		return total/orders.size();
	}
}