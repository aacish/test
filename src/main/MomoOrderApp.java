package main;

import input.ImportOrders;
import input.OrderComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import algorithm.FindWaitingTime;
import entity.Order;

public class MomoOrderApp {
	
	
	public static void main(String[] args) {
		
		//input is in input.txt file
		ImportOrders o = new ImportOrders();
		List<Order> orders = o.impot("input.txt");
		System.out.println("==================================================");
		System.out.println("optimized queue");
		System.out.println("------------------");
		
		//Sort the orders by servicetime ascending
		List<Order> first = new ArrayList<Order>();
		first.add(orders.get(0));
		List<Order> subList = orders.subList(1, orders.size());
		Collections.sort(subList,new OrderComparator());
		first.addAll(subList);
		orders=first;
		FindWaitingTime waitingTime = new FindWaitingTime();
		List<Order> aftW = waitingTime.populateWaitingTime(orders);
		
		for(Order order : aftW) {
			System.out.println(order.toString());
		}
		
		System.out.println("==============================================");
		
		Double average = waitingTime.getAverageWaitingTime(orders);
		System.out.println("Average waiting time: "+average.intValue());
		
	}
	
	
}
