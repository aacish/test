package entity;

public class Order {
	private long arrTime=0;
	private Double execDuration=0.0;
	private long endTime = 0;
	private long waitingTime=0;
	
	public long getArrTime() {
		return arrTime;
	}
	public void setArrTime(long arrTime) {
		this.arrTime = arrTime;
	}
	public Double getExecDuration() {
		return execDuration;
	}
	public void setExecDuration(Double execDuration) {
		this.execDuration = execDuration;
	}
	public long getWaitingTime() {
		return waitingTime;
	}
	public void setWaitingTime(long waitingTime) {
		this.waitingTime = waitingTime;
	}
	
	public String toString() {
		return "Arrival: " + this.arrTime + "    | Execution Time: " + execDuration + "    | Waiting time: " + this.waitingTime + "    |End Time: "+this.endTime;
	}
	public long getEndTime() {
		return endTime;
	}
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	
}
