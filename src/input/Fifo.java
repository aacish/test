package input;

import java.util.Comparator;

import entity.Order;

public class Fifo implements Comparator<Order> {

	public int compare(Order o1, Order o2) {
		Double d1 = (double)o1.getArrTime();
		Double d2 = (double)o2.getArrTime();
		return d1.compareTo(d2);
	}

}
