package input;

import java.util.Comparator;

import entity.Order;

public class OrderComparator implements Comparator<Order> {

	public int compare(Order o1, Order o2) {
		return o1.getExecDuration().compareTo(o2.getExecDuration());
	}

}
