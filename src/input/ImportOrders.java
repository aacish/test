package input;


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import entity.*;

public class ImportOrders {
	
	public List<Order> impot(String file) {
		List<Order> orders = new ArrayList<Order>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			String n = reader.readLine();
			System.out.println("Total orders: "+n);
			
			while((line=reader.readLine())!=null) {
				Order order = new Order();
				String arr[] = line.split("[ ]+");
				long arrival = 0;
				Double exec = 0.0;
				if(arr.length>0) {
					arrival = Long.valueOf(arr[0]);
					exec = Double.valueOf(arr[1]);
				}
				
				order.setArrTime(arrival);
				order.setExecDuration(exec);
				orders.add(order);
			}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		return orders;
	}

}
